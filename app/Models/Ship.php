<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ship extends Model
{
    use HasFactory;

    protected $fillable = ['serie', 'model', 'capacity', 'description', 'created_by'];

    public function createdBy(){
        return $this->belongsTo(User::class,'created_by');
    }
}
