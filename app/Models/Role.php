<?php

namespace App\Models;

use Illuminate\Support\Facades\DB;
use Trebol\Entrust\EntrustRole;

/**
 * Class Role
 * @package App
 */
class Role extends EntrustRole
{

    /**
     * @return int|mixed
     */
    protected $hidden = array('pivot');

    public function getTotalUsers()
    {
        if (is_null($this->totalUsers)) {
            $this->totalUsers = DB::table('role_user')->where('role_id', '=', $this->id)->count();
        }
        return $this->totalUsers;
    }
}
