<?php

namespace App\Http\Controllers\Api\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

trait IssueTokenTrait
{

    public function issueToken(Request $request, $grantType, $scope = '*')
    {
        if (!$this->client){
            return response()->json([
                "error" => "invalid_client",
                "error_description" => "Client passport not installed.",
                "message" => "Client passport not installed."
            ], 500);
        }
        $params = [
            'grant_type' => $grantType,
            'client_id' => $this->client->id,
            'client_secret' => $this->client->secret,
            'username' => $request->input('username') ?: $request->input('email'),
            'scope' => $scope,
        ];

        $request->request->add($params);

        $proxy = Request::create('oauth/token', 'POST');

        return Route::dispatch($proxy);
    }
}
