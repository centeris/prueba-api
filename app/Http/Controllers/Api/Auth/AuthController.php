<?php

namespace App\Http\Controllers\Api\Auth;

use App\Log;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;
use Laravel\Passport\Client;

class AuthController extends Controller
{
    use IssueTokenTrait;

    private $client;

    public function __construct()
    {
        $this->client = Client::where('personal_access_client', false)
            ->where('revoked', false)
            ->orderByDesc('id')
            ->first();
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required',
            'password' => 'required',
        ]);

        if (!User::where('email', $request->input('email'))->count())
            return response()->json([
                "error" => "user_not_found",
                "error_description" => "User not found",
                "message" => "User not found"
            ], 401);

        $response = $this->issueToken($request, 'password');

        if ($response->getStatusCode() != 200) {
            $data = (json_decode($response->content()));
            if ($data->message == 'The user credentials were incorrect.')
                return response()->json([
                    "error" => "invalid_credentials",
                    "error_description" => "The user credentials are incorrect",
                    "message" => "Wrong password"
                ], 401);

            return response()->json(json_decode($response->content()), $response->getStatusCode());
        }

        $accessToken = json_decode((string)$response->content(), true)['access_token'];

        $request->headers->set('Authorization', 'Bearer ' . $accessToken);
        $user = \Route::dispatch(Request::create('/api/profile', 'GET'));

        if ($user->content()) {
            $response = json_decode($response->content(), true);
            $response["user"] = json_decode($user->content(), true);
        } else {
            return response()->json([
                "error" => "invalid_credentials",
                "error_description" => "The user credentials are incorrect",
                "message" => "The user credentials are incorrect"
            ], 401);
        }
        return response()->json($response, 200);
    }

    public function refresh(Request $request)
    {
        $this->validate($request, [
            'refresh_token' => 'required',
        ]);

        return $this->issueToken($request, 'refresh_token');
    }

    public function logout()
    {
        $accessToken = \Auth::user()->token();

        \DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update(['revoked' => true]);

        $accessToken->revoke();

        return response()->json(["message" => "Se sesión ha finalizado"], 200);
    }

    public function register(Request $request)
    {
        $role = Role::find(3);

        if (!$role) {
            return response()->json([
                'response' => 'error',
                'data' => [],
                'error' => "Role not found"
            ], 200);
        }

        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }

        $user = User::create([
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => \Hash::make($request->input('password')),
        ]);
        $user->attachRole($role);

        $response = $this->issueToken($request, 'password');
        $response = json_decode($response->content(), true);
        $response["user"] = $user;

        return response()->json($response, 200);
    }

    public function profile()
    {
        return User::with(['roles.perms'])
            ->where('id', auth()->id())
            ->first();
    }

    public function updateProfile(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . auth()->id(),
            'address' => 'array',
            'address.*.address' => "required",
            'address.*.city' => "required",
            'address.*.state' => "required",
            'address.*.zipcode' => "required",
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }
        $user = auth()->user();
        $user->update([
            'name' => $request->input('name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'mobile' => $request->input('mobile'),
        ]);

        if ($request->has('address')) {
            foreach ($request->input('address') as $item) {
                UserAddress::updateOrCreate(
                    [
                        'user_id' => auth()->id(),
                        'id' => array_key_exists('id', $item) ? $item['id'] : null
                    ],
                    [
                        'address' => $item['address'],
                        'city' => $item['city'],
                        'state' => $item['state'],
                        'zipcode' => $item['zipcode'],
                        'comment' => array_key_exists('comment', $item) ? $item['comment'] : null,
                    ]
                );
            }
        }

        if ($request->input('password')) {
            $user->update([
                'password' => \Hash::make($request->input('password')),
            ]);
        }

        return response()->json([
            'response' => 'success',
            'data' => [
                'user' => User::with('address')
                    ->where('id', auth()->id())
                    ->first()
            ],
            'error' => null
        ], 200);
    }
}
