<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller
{
    public function getUsers(Request $request)
    {
        return response()->json([
            "response" => "success",
            "data" => User::get(),
            "error" => null
        ]);
    }

    public function create(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }

        try {
            \DB::beginTransaction();
            $user = User::create([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);

            $user->roles()->sync($request->input('role_id'));
            \DB::commit();
            return response()->json([
                'response' => 'success',
                'data' => $user,
                'error' => null
            ], 200);
        } catch (\Exception $ex) {
            \DB::rollback();
        }
    }

    public function update(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,id,' . $request->input('id'),
            'password' => 'nullable|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }

        try {
            \DB::beginTransaction();
            $user = User::find($request->input('id'));

            $user->update([
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'password' => bcrypt($request->input('password')),
            ]);

            if ($request->input('password'))
                $user->update([
                    'password' => bcrypt($request->input('password')),
                ]);

            $user->roles()->sync($request->input('role_id'));
            \DB::commit();
            return response()->json([
                'response' => 'success',
                'data' => $user,
                'error' => null
            ], 200);
        } catch (\Exception $ex) {
            \DB::rollback();
        }
    }

    public function getUser(User $user)
    {
        return response()->json([
            "response" => "success",
            "data" => $user,
            "error" => null
        ]);
    }

    public function destroy(User $user)
    {
        if (auth()->id() != $user->id()) {
            $user->delete();
            return response()->json([
                "response" => "success",
                "data" => $user,
                "error" => null
            ]);
        }
        return response()->json([
            "response" => "error",
            "data" => $user,
            "error" => null
        ], 406);

    }
}
