<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Ship;
use App\Models\User;
use Illuminate\Http\Request;

class ShipController extends Controller
{
    public function getShips(){
        return response()->json([
            "response" => "success",
            "data" => Ship::get(),
            "error" => null
        ]);
    }

    public function create(Request $request){
        $validator = \Validator::make($request->all(), [
            'serie' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'capacity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }

        $ship = Ship::create([
            'serie' => $request->input('serie'),
            'model' => $request->input('model'),
            'capacity' => $request->input('capacity'),
            'description' => $request->input('description'),
            'created_by' => auth()->id()
        ]);
        return response()->json([
            'response' => 'success',
            'data' => $ship,
            'error' => null
        ], 200);
    }


    public function update(Request $request){
        $validator = \Validator::make($request->all(), [
            'serie' => 'required|string|max:255',
            'model' => 'required|string|max:255',
            'capacity' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'response' => 'error',
                'data' => null,
                'error' => $validator->messages()
            ], 406);
        }

        $ship = Ship::find($request->input('id'));
        $ship->update([
            'serie' => $request->input('serie'),
            'model' => $request->input('model'),
            'capacity' => $request->input('capacity'),
            'description' => $request->input('description'),
            'created_by' => auth()->id()
        ]);
        return response()->json([
            'response' => 'success',
            'data' => $ship,
            'error' => null
        ], 200);
    }


    public function destroy(Ship $ship){

        $ship->delete();
        return response()->json([
            "response" => "success",
            "data" => $ship,
            "error" => null
        ]);
    }

    public function getShip(Ship $ship){
        return response()->json([
            "response" => "success",
            "data" => $ship,
            "error" => null
        ]);
    }
}
