<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\UserController;
use \App\Http\Controllers\Api\ShipController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace' => 'Api'], function () {
Route::post('/login', [AuthController::class, 'login']);
Route::post('/register', [AuthController::class, 'register']);
Route::group(['middleware' => ['auth:api']], function () {
    Route::get('/profile', [AuthController::class, 'profile']);
    Route::get('/users', [UserController::class, 'getUsers']);
    Route::post('/user/create', [UserController::class, 'create']);
    Route::post('/user/update', [UserController::class, 'update']);
    Route::delete('/user/{user}/delete', [UserController::class, 'destroy']);
    Route::get('/user/{user}/get', [UserController::class, 'getUser']);

    Route::get('/ships', [ShipController::class, 'getShips']);
    Route::post('/ship/create', [ShipController::class, 'create']);
    Route::post('/ship/update', [ShipController::class, 'update']);
    Route::delete('/ship/{ship}/delete', [ShipController::class, 'destroy']);
    Route::get('/ship/{ship}/get', [ShipController::class, 'getShip']);
});
});
